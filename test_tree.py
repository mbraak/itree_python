from unittest import TestCase
import unittest

from itree import Tree


data1 = [
    dict(
        name='n1',
        id=1,
        children=[
            dict(name='n1a', id=2),
            dict(name='n1b', id=3)
        ]
    ),
    dict(
        name='n2',
        id=4,
        children=[
            dict(name='n2a', id=5),
            dict(name='n2b', id=6)
        ]
    )
]


class TreeTests(TestCase):
    def test_create_empty_tree(self):
        t = Tree()

        self.assertEqual("", t.to_string())

    def test_create_tree_from_data(self):
        t = Tree(data1)

        self.assertEqual("n1(n1a n1b) n2(n2a n2b)", t.to_string())

    def test_add_node(self):
        t1 = Tree()
        t2 = t1.add(dict(name='n1', id=1))

        self.assertEqual("", t1.to_string())
        self.assertEqual("n1", t2.to_string())

    def test_get_node_by_name(self):
        t = Tree(data1)
        n2a = t.get_node_by_name('n2a')

        self.assertEqual('n2a', n2a['name'])
        self.assertEqual(5, n2a['id'])

    def test_add_child_node(self):
        t1 = Tree()\
            .add(dict(name='n1', id=1))\
            .add(dict(name='n2', id=2))

        n1 = t1.get_node_by_name('n1')
        t2 = t1.add(n1, dict(name='n1a', id=3))

        self.assertEqual("n1 n2", t1.to_string())
        self.assertEqual("n1(n1a) n2", t2.to_string())

    def test_remove_node(self):
        t1 = Tree()\
            .add(dict(name='n1', id=1))\
            .add(dict(name='n2', id=2))

        n1 = t1.get_node_by_name('n1')
        t2 = t1.remove(n1)

        self.assertEqual("n1 n2", t1.to_string())
        self.assertEqual("n2", t2.to_string())

        self.assertIsNone(t2.get_node_by_id(1))

    def test_remove_node_with_children(self):
        t1 = Tree(data1)
        n2 = t1.get_node_by_name('n2')
        t2 = t1.remove(n2)

        self.assertEqual("n1(n1a n1b)", t2.to_string())
        self.assertIsNone(t2.get_node_by_id(4))
        self.assertIsNone(t2.get_node_by_id(5))

    def test_remove_child_node(self):
        t1 = Tree(data1)

        n1a = t1.get_node_by_name('n1a')
        t2 = t1.remove(n1a)

        n1 = t2.get_node_by_name('n1')
        t3 = t2.remove(n1)

        self.assertEqual("n1(n1a n1b) n2(n2a n2b)", t1.to_string())
        self.assertEqual("n1(n1b) n2(n2a n2b)", t2.to_string())
        self.assertIsNone(t2.get_node_by_id(2))
        self.assertEqual("n2(n2a n2b)", t3.to_string())
        self.assertSetEqual({4, 5, 6}, set(t3.ids.keys()))

    def test_get_node_by_id(self):
        t1 = Tree(data1)
        t2 = t1.add(dict(name='n3', id=7))

        n2a = t2.get_node_by_name('n2a')
        t3 = t2.remove(n2a)

        self.assertEqual("n1a", t1.get_node_by_id(2)['name'])
        self.assertEqual("n3", t2.get_node_by_id(7)['name'])
        self.assertIsNone(t3.get_node_by_id(5))


if __name__ == '__main__':
    unittest.main()
