from typing import Iterable, Mapping


class PVector(Iterable):
    def append(self, val) -> PVector:
        pass

    def index(self, value, *args, **kwargs):
        pass

    def set(self, i, val) -> PVector:
        pass

    def remove(self, value) -> PVector:
        pass


def pvector(iterable=()) -> PVector:
    pass


class PMap(Mapping):
    def get(self, key, default=None):
        pass

    def set(self, key, val) -> PMap:
        pass

    def discard(self, key) -> PMap:
        pass

    def remove(self, key) -> PMap:
        pass

    def update(self, *maps) -> PMap:
        pass


def m(**kwargs) -> PMap:
    pass


def pmap(initial={}, pre_size=0) -> PMap:
    pass


class PList:
    pass


def l(*elements) -> PList:
    pass


def thaw(o):
    pass
