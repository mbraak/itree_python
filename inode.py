from typing import List, Optional, Tuple

from pyrsistent import pvector, m, pmap, l, PMap, PVector


Node = PMap


def create(children_data: List[dict]=None) -> Node:
    if not children_data:
        return m(name=None, id=None, is_root=True)
    else:
        return create()\
            .set(
                'children', _create_nodes_from_data(None, children_data)
            )


def _create_nodes_from_data(parent_id: Optional[int], children_data: List[dict]) -> PVector:
    return pvector(
        _create_node_from_data(parent_id, node) for node in children_data
    )


def _create_node_from_data(parent_id: Optional[int], node_data: dict) -> Node:
    node = pmap(node_data)\
        .set('parent_id', parent_id)  # type: Node

    children_data = node_data.get('children')  # type: Optional[List[dict]]

    if not children_data:
        return node.discard('children')
    else:
        return node\
            .set(
                'children',
                _create_nodes_from_data(
                    node_data.get('id'),
                    children_data
                )
            )


def to_string(node: Node) -> str:
    children = node.get('children', [])  # type: PVector

    if node.get('is_root'):
        return _nodes_to_string(children)
    else:
        name = node.get('name', '')  # type: str

        if not children:
            return name
        else:
            return '{}({})'.format(name, _nodes_to_string(children))


def _nodes_to_string(nodes: PVector) -> str:
    return " ".join(
        to_string(node) for node in nodes
    )


def get_node_by_name(root: Node, name: str) -> Optional[Node]:
    for ro_node in _iterate_tree_with_parents(root):
        if ro_node['node'].get('name') == name:
            return ro_node

    return None


def _iterate_tree_with_parents(node: Node, path=None):
    """Iterate tree; return lazy sequence of readonly nodes"""
    if not node.get('is_root'):
        yield dict(node=node, parents=path)

    children = node.get('children')  # type: PVector

    if children:
        new_path = l(node) if path is None else path.cons(node)

        for child in children:
            for ro_node in _iterate_tree_with_parents(child, new_path):
                yield ro_node


def iterate_tree(node: Node):
    if not node.get('is_root'):
        yield node

    children = node.get('children')

    if children:
        for child in children:
            yield from iterate_tree(child)


def add_node(root: Node, param1, param2=None) -> Tuple[Node, dict]:
    """
    Add node

    return [new_root {new_child changed_nodes}]
    """
    if param2:
        return _add_child_node(param1, param2)
    else:
        return _add_node_to_root(root, param1)


def _add_child_node(readonly_parent: dict, child_data: dict) -> Tuple[Node, dict]:
    """
    Add node to parent; parent must not be the root node
    """
    parent = readonly_parent['node']
    assert_node_is_not_root(parent, '_add_child_node')

    child = pmap(child_data)\
        .set('parent_id', parent['id'])

    new_parent = _add_child(parent, child)

    new_root, changed_nodes = _update_parents(parent, new_parent, readonly_parent['parents'])

    changed_nodes.append(new_parent)
    return new_root, dict(new_child=child, changed_nodes=changed_nodes)


def _add_node_to_root(root: Node, child_data: dict) -> Tuple[Node, dict]:
    child = pmap(child_data)
    new_root = _add_child(root, child)

    return new_root, dict(new_child=child, changed_nodes=[])


def _add_child(node: Node, child: Node) -> Node:
    new_children = _get_children(node).append(child)

    return node.set('children', new_children)


def _update_parents(old_child: Node, new_child: Node, parents: List[Node]) -> Tuple[Node, List]:
    """
    Update parent of updated node; also update the parents of the parent

    - 'old_child' is replaced by 'new_child'
    - 'parents' are the parents of the child; direct parent first
    - returns: [new root, affected]
    """
    assert_node_is_not_root(old_child, '_update_parents')
    assert len(parents) > 0, '_update_parents: parents must not be empty; old_child is %s' % old_child.get('id')
    assert parents[-1].get('is_root'), '_update_parents: last parent must be root; %s' % parents[-1]

    new_parents = []

    for parent in parents:
        new_parent = _replace_child(parent, old_child, new_child)

        new_parents.append(new_parent)

        old_child = parent
        new_child = new_parent

    new_root = new_parents[-1]  # type: Node
    assert new_root.get('is_root'), '_update_parents: new root must be root; %s' % new_root

    affected_nodes = new_parents[:-1]

    return new_root, affected_nodes


def _get_children(node: Node) -> PVector:
    if 'children' in node:
        return node['children']
    else:
        return pvector()


def _replace_child(node: Node, old_child: Node, new_child: Node) -> Node:
    children = _get_children(node)
    child_index = children.index(old_child)
    new_children = children.set(child_index, new_child)

    return node.set('children', new_children)


def remove_node(ro_node: dict):
    """
    Remove node
    - return [new_root {changed_nodes removed_nodes}]
    """
    parents = ro_node['parents']
    parent = parents[0]
    node = ro_node['node']

    if parent.get('is_root'):
        return _remove_node_from_root(parent, node)
    else:
        return _remove_node_from_parent(parents, node)


def _remove_node_from_root(root: Node, child: Node) -> Tuple[Node, dict]:
    new_root = _remove_child(root, child)

    removed_nodes = iterate_tree(child)

    return new_root, dict(changed_nodes=[], removed_nodes=removed_nodes)


def _remove_node_from_parent(parents: List[Node], child: Node) -> Tuple[Node, dict]:
    parent = parents[0]

    new_parent = _remove_child(parent, child)

    new_root, changed_nodes = _update_parents(parent, new_parent, parents[1:])

    removed_nodes = list(iterate_tree(child))
    changed_nodes.append(new_parent)

    return new_root, dict(changed_nodes=changed_nodes, removed_nodes=removed_nodes)


def _remove_child(node: Node, child: Node) -> Node:
    new_children = _get_children(node).remove(child)

    return node.set('children', new_children)


def update_node(ro_node: dict, attributes: dict) -> Tuple[Node, List]:
    node = ro_node['node']
    new_node = node.update(attributes)

    new_root, changed_nodes = _update_parents(node, new_node, ro_node['parents'])

    changed_nodes.append(new_node)

    return new_root, changed_nodes


def assert_node_is_not_root(node: Node, function_name: str):
    assert not node.get('is_root'), '%s: node must not be root' % function_name
