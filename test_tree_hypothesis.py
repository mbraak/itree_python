import unittest
from unittest import TestCase

from hypothesis.searchstrategy import SearchStrategy
from hypothesis.strategies import text, cacheable, defines_strategy, choices
from hypothesis.stateful import Bundle, RuleBasedStateMachine, rule, run_state_machine_as_test, precondition
from hypothesis import settings, Verbosity

import treelib
from pyrsistent import thaw

import itree


class NodeIdStrategy(SearchStrategy):
    def __init__(self):
        super(SearchStrategy, self).__init__()

        self.new_id = 0

    def do_draw(self, data):
        self.new_id += 1

        return self.new_id


@cacheable
@defines_strategy
def node_ids():
    return NodeIdStrategy()


class Transient:
    def __init__(self, tree=None):
        self.tree = tree if tree else self._create()

    def _create(self):
        t = treelib.Tree()
        t.create_node('', 0)

        return t

    def add(self, node_id, name, parent_id):
        tree = self._copy_tree()

        tree.create_node(name, node_id, parent_id)

        return Transient(tree)

    def _copy_tree(self):
        return treelib.Tree(self.tree, True)

    def remove(self, node_id):
        tree = self._copy_tree()

        tree.remove_node(node_id)

        return Transient(tree)

    def update(self, node_id, name):
        tree = self._copy_tree()

        tree.get_node(node_id).tag = name

        return Transient(tree)

    def get_ids(self):
        ids = list(self.tree.nodes.keys())
        ids.remove(0)

        return ids

    def to_string(self):
        def node_to_string(node):
            node_id = node.identifier

            children = self.tree.children(node_id)

            if node_id == 0:
                return nodes_to_string(children)
            else:
                if not children:
                    return node.tag
                else:
                    return '{}({})'.format(node.tag, nodes_to_string(children))

        def nodes_to_string(nodes):
            return " ".join(
                node_to_string(node) for node in nodes
            )

        root = self.tree.get_node(0)
        return node_to_string(root)


class Persistent:
    def __init__(self, tree=None):
        self.tree = tree if tree else itree.Tree()

    def get_random_node_id(self, choice):
        ids = self.tree.ids.keys().append(0)

        return choice(ids)

    def add(self, node_id, name, parent_id):
        child = dict(name=name, id=node_id)

        if parent_id == 0:
            tree = self.tree.add(child)
        else:
            parent = self.tree.get_node_by_id(parent_id)
            tree = self.tree.add(parent, child)

        return Persistent(tree)

    def remove(self, node_id):
        node = self.tree.get_node_by_id(node_id)
        assert node, 'Persistent.remove: node %s not found' % node_id

        tree = self.tree.remove(node)
        assert tree.get_node_by_id(node_id) is None, 'Persistent.remove: node should be removed'

        return Persistent(tree)

    def update(self, node_id, name):
        node = self.tree.get_node_by_id(node_id)
        tree = self.tree.update(node, dict(name=name))

        return Persistent(tree)

    def get_ids(self):
        return thaw(self.tree.ids.keys())

    def to_string(self):
        return self.tree.to_string()


def create_initial_tree_pair():
    return TreePair(
        Persistent(),
        Transient()
    )


class TreePair:
    def __init__(self, persistent, transient):
        self.persistent = persistent
        self.transient = transient

    def get_random_node_id(self, choice):
        return self.persistent.get_random_node_id(choice)

    def add(self, node_id, name, parent_id):
        return TreePair(
            self.persistent.add(node_id, name, parent_id),
            self.transient.add(node_id, name, parent_id)
        )

    def remove(self, node_id):
        return TreePair(
            self.persistent.remove(node_id),
            self.transient.remove(node_id)
        )

    def update(self, node_id, name):
        return TreePair(
            self.persistent.update(node_id, name),
            self.transient.update(node_id, name)
        )

    def check(self):
        def check_ids():
            persistent_ids = self.persistent.get_ids()
            transient_ids = self.transient.get_ids()

            assert set(persistent_ids) == set(transient_ids), 'ids should be equal: %s != %s' % (persistent_ids, transient_ids)

        def check_trees():
            persistent_string = self.persistent.to_string()
            transient_string = self.transient.to_string()

            assert persistent_string == transient_string, '%s != %s' % (persistent_string, transient_string)

        check_ids()
        check_trees()


class PersistentTreeBuilder(RuleBasedStateMachine):
    tree_pair = Bundle('tree_pair')

    # condition: only start if this is the first step
    @precondition(lambda self: not self.bundles['tree_pair'])
    @rule(target=tree_pair)
    def initial(self):
        return create_initial_tree_pair()

    @rule(target=tree_pair, tree_pair=tree_pair, name=text(min_size=2), new_id=node_ids(), parent_id_choice=choices())
    def add(self, tree_pair, name, new_id, parent_id_choice):
        parent_id = tree_pair.get_random_node_id(parent_id_choice)

        return tree_pair.add(new_id, name, parent_id)

    @rule(target=tree_pair, tree_pair=tree_pair, node_id_choice=choices())
    def remove(self, tree_pair, node_id_choice):
        node_id = tree_pair.get_random_node_id(node_id_choice)

        if not node_id:
            return tree_pair
        else:
            return tree_pair.remove(node_id)

    @rule(target=tree_pair, tree_pair=tree_pair, node_id_choice=choices(), new_name=text(min_size=2))
    def update(self, tree_pair, node_id_choice, new_name):
        node_id = tree_pair.get_random_node_id(node_id_choice)

        if not node_id:
            return tree_pair
        else:
            return tree_pair.update(node_id, new_name)

    @rule(tree_pair=tree_pair)
    def check(self, tree_pair):
        tree_pair.check()


class PersistentTreeTests(TestCase):
    def test_tree(self):
        run_state_machine_as_test(
            PersistentTreeBuilder,
            settings(verbosity=Verbosity.normal, max_examples=2000),
        )


if __name__ == '__main__':
    unittest.main()
