from typing import Iterable
from unittest import TestCase

from pyrsistent import thaw

import inode


data1 = [
    dict(
        name='n1',
        id=1,
        children=[
            dict(name='n1a', id=2),
            dict(name='n1b', id=3)
        ]
    ),
    dict(
        name='n2',
        id=4,
        children=[
            dict(name='n2a', id=5),
            dict(name='n2b', id=6)
        ]
    )
]


def nodes_to_ids(nodes):
    return set(n['id'] for n in nodes)


def node_list_to_string(nodes: Iterable) -> str:
    return " ".join(
        '[root]' if node.get('is_root') else node.name
        for node in nodes
    )


class NodeTests(TestCase):
    def test_create(self):
        t1 = inode.create()

        self.assertEqual("", inode.to_string(t1))

    def test_create_from_data(self):
        t1 = inode.create(data1)

        self.assertEqual("n1(n1a n1b) n2(n2a n2b)", inode.to_string(t1))

        n1a = inode.get_node_by_name(t1, "n1a")

        self.assertEqual(1, n1a['node']['parent_id'])

    def test_add_node(self):
        t1 = inode.create()

        t2, changed_t2 = inode.add_node(t1, dict(name='n1', id=1))

        n1 = inode.get_node_by_name(t2, "n1")

        t3, changed_t3 = inode.add_node(t2, n1, dict(name='n1a', id=2))

        n1a = inode.get_node_by_name(t3, 'n1a')

        t4, changed_t4 = inode.add_node(t3, n1a, dict(name='n1b', id=3))

        self.assertEqual("n1", inode.to_string(t2))
        self.assertSetEqual({'new_child', 'changed_nodes'}, set(changed_t2.keys()))
        self.assertEqual([], changed_t2['changed_nodes'])
        self.assertEqual("n1(n1a)", inode.to_string(t3))
        self.assertTrue(t3['is_root'])
        self.assertEqual("n1", inode.node_list_to_string(changed_t3['changed_nodes']))
        self.assertEqual(dict(id=2, parent_id=1, name='n1a'), thaw(changed_t3['new_child']))
        self.assertEqual(1, n1a['node']['parent_id'])
        self.assertEqual("n1(n1a(n1b))", inode.to_string(t4))

    def test_get_node_by_name(self):
        t1 = inode.create(data1)
        n1a = inode.get_node_by_name(t1, 'n1a')

        self.assertEqual('n1a', n1a['node']['name'])
        self.assertEqual('n1 [root]', inode.node_list_to_string(n1a['parents']))

    def test_remove_node(self):
        # t1: example tree
        t1 = inode.create(data1)

        # t2: remove n1
        n1 = inode.get_node_by_name(t1, 'n1')
        t2, info_t2 = inode.remove_node(n1)

        # t3: remove n2a
        n2a = inode.get_node_by_name(t2, 'n2a')
        t3, info_t3 = inode.remove_node(n2a)

        # t4: add n2c
        n2b = inode.get_node_by_name(t3, 'n2b')
        t4, info_t4 = inode.add_node(t3, n2b, dict(name='n2c', id=7))

        # t5: remove n2c
        n2c = inode.get_node_by_name(t4, 'n2c')
        t5, info_t5 = inode.remove_node(n2c)

        self.assertEqual("n2(n2a n2b)", inode.to_string(t2))
        self.assertEqual([], info_t2['changed_nodes'])
        self.assertEqual({1, 2, 3}, nodes_to_ids(info_t2['removed_nodes']))
        self.assertEqual("n2(n2b)", inode.to_string(t3))
        self.assertEqual({4}, nodes_to_ids(info_t3['changed_nodes']))
        self.assertEqual({5}, nodes_to_ids(info_t3['removed_nodes']))
        self.assertEqual("n2(n2b(n2c))", inode.to_string(t4))
        self.assertEqual("n2(n2b)", inode.to_string(t5))

    def test_update_node(self):
        t1 = inode.create(data1)

        n2a = inode.get_node_by_name(t1, 'n2a')
        t2, update_t2 = inode.update_node(n2a, dict(name='N2A', color='green'))
