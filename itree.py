from typing import List, Optional, cast

from pyrsistent import PMap, pmap

import inode
from inode import Node


class Tree:
    def __init__(self, data: List[dict]=None) -> None:
        root = inode.create(data)

        self.root = root
        self.selected = None
        self.ids = _create_id_map(root)

    def to_string(self) -> str:
        return inode.to_string(self.root)

    def add(self, param1, param2=None) -> 'Tree':
        if not param2:
            return self._add_to_root(param1)
        else:
            return self._add_to_parent(param1, param2)

    def _add_to_root(self, child: dict) -> 'Tree':
        new_root, update_info = inode.add_node(self.root, child)

        new_child = update_info['new_child']

        return self._clone_tree(
            new_root,
            [new_child],
            []
        )

    def _add_to_parent(self, parent: Node, child: dict) -> 'Tree':
        inode.assert_node_is_not_root(parent, '_add_to_parent')

        ro_parent = self._get_readonly_node(parent)

        new_root, update_info = inode.add_node(self.root, ro_parent, child)

        changed_nodes = update_info['changed_nodes']
        new_child = update_info['new_child']
        changed_nodes.append(new_child)

        return self._clone_tree(
            new_root,
            changed_nodes,
            []
        )

    def _clone_tree(self, new_root: Node, updated_nodes: List[Node], deleted_ids: List[int]) -> 'Tree':
        assert new_root.get('is_root'), '_clone_tree: new_root must be root; %s' % new_root

        new_ids = self._update_ids(updated_nodes, deleted_ids)

        new_tree = Tree()
        new_tree.root = new_root
        new_tree.selected = self.selected
        new_tree.ids = new_ids

        return new_tree

    def _update_ids(self, updated_nodes: List[Node], deleted_ids: List[int]) -> PMap:
        # get ids from updated nodes
        # - skip root node
        updated_node_map = {n['id']: n for n in updated_nodes if not n.get('is_root')}

        new_ids = self.ids\
            .update(updated_node_map)

        for node_id in deleted_ids:
            new_ids = new_ids.remove(node_id)

        return new_ids

    def get_node_by_name(self, name: str) -> Optional[dict]:
        ro_node = inode.get_node_by_name(self.root, name)

        if not ro_node:
            return None
        else:
            return ro_node['node']

    def _get_readonly_node(self, node: Node) -> dict:
        return dict(
            node=node,
            parents=self._get_parents(node)
        )

    def _get_parents(self, node: Node) -> List[Node]:
        """
        return parents; direct parent first
        """
        def get_parent(n: Node) -> Optional[Node]:
            parent_id = n.get('parent_id')

            assert not n.get('is_root')

            if not parent_id:
                return self.root
            else:
                return self.get_node_by_id(parent_id)

        parents = []
        n = node

        while n and (not n.get('is_root')):
            parent = get_parent(n)

            assert parent != n, '_get_parents: node cannot be its own parent; %s' % node

            if parent:
                parents.append(parent)

            n = cast(Node, parent)

        assert n.get('is_root') or len(parents) > 0, '_get_parents: no parents found for node %s' % node.get('id')

        return parents

    def remove(self, node: Node) -> 'Tree':
        ro_node = self._get_readonly_node(node)

        new_root, affected_info = inode.remove_node(ro_node)

        deleted_ids = [n['id'] for n in affected_info['removed_nodes']]

        return self._clone_tree(
            new_root,
            affected_info['changed_nodes'],
            deleted_ids
        )

    def get_node_by_id(self, node_id: int) -> Optional[Node]:
        return self.ids.get(node_id)

    def update(self, node: Node, attributes: dict) -> 'Tree':
        new_root, changed_nodes = inode.update_node(
            self._get_readonly_node(node),
            attributes
        )

        return self._clone_tree(
            new_root,
            changed_nodes,
            []
        )


def _create_id_map(root: Node) -> PMap:
    return pmap({
        node['id']: node for node in inode.iterate_tree(root)
    })
